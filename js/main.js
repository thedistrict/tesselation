/*
	JS to control SVG

		Will also control placement of all links using data-x and data-y and even data-width


*/
/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */
if("document" in self){if(!("classList" in document.createElement("_"))){(function(j){"use strict";if(!("Element" in j)){return}var a="classList",f="prototype",m=j.Element[f],b=Object,k=String[f].trim||function(){return this.replace(/^\s+|\s+$/g,"")},c=Array[f].indexOf||function(q){var p=0,o=this.length;for(;p<o;p++){if(p in this&&this[p]===q){return p}}return -1},n=function(o,p){this.name=o;this.code=DOMException[o];this.message=p},g=function(p,o){if(o===""){throw new n("SYNTAX_ERR","An invalid or illegal string was specified")}if(/\s/.test(o)){throw new n("INVALID_CHARACTER_ERR","String contains an invalid character")}return c.call(p,o)},d=function(s){var r=k.call(s.getAttribute("class")||""),q=r?r.split(/\s+/):[],p=0,o=q.length;for(;p<o;p++){this.push(q[p])}this._updateClassName=function(){s.setAttribute("class",this.toString())}},e=d[f]=[],i=function(){return new d(this)};n[f]=Error[f];e.item=function(o){return this[o]||null};e.contains=function(o){o+="";return g(this,o)!==-1};e.add=function(){var s=arguments,r=0,p=s.length,q,o=false;do{q=s[r]+"";if(g(this,q)===-1){this.push(q);o=true}}while(++r<p);if(o){this._updateClassName()}};e.remove=function(){var t=arguments,s=0,p=t.length,r,o=false,q;do{r=t[s]+"";q=g(this,r);while(q!==-1){this.splice(q,1);o=true;q=g(this,r)}}while(++s<p);if(o){this._updateClassName()}};e.toggle=function(p,q){p+="";var o=this.contains(p),r=o?q!==true&&"remove":q!==false&&"add";if(r){this[r](p)}if(q===true||q===false){return q}else{return !o}};e.toString=function(){return this.join(" ")};if(b.defineProperty){var l={get:i,enumerable:true,configurable:true};try{b.defineProperty(m,a,l)}catch(h){if(h.number===-2146823252){l.enumerable=false;b.defineProperty(m,a,l)}}}else{if(b[f].__defineGetter__){m.__defineGetter__(a,i)}}}(self))}else{(function(){var b=document.createElement("_");b.classList.add("c1","c2");if(!b.classList.contains("c2")){var c=function(e){var d=DOMTokenList.prototype[e];DOMTokenList.prototype[e]=function(h){var g,f=arguments.length;for(g=0;g<f;g++){h=arguments[g];d.call(this,h)}}};c("add");c("remove")}b.classList.toggle("c3",false);if(b.classList.contains("c3")){var a=DOMTokenList.prototype.toggle;DOMTokenList.prototype.toggle=function(d,e){if(1 in arguments&&!this.contains(d)===!e){return e}else{return a.call(this,d)}}}b=null}())}};


var SVG = function(d,w){
	var svgFix = function(div, svg, d, w ){
		var mH = (d.documentElement.scrollHeight < w.innerHeight)?w.innerHeight:d.documentElement.scrollHeight;
		var mW = (d.documentElement.scrollWidth < w.innerWidth)?d.documentElement.scrollWidth:w.innerWidth;
		var holder = d.querySelector('section');
		svg.setAttribute('width',mW);
		var screen = 'desktop';
		if (window.innerWidth < 600){
			screen = 'mobile';
		}else if (window.innerWidth < 960){
			screen = 'tablet';
		}
		var style = getComputedStyle(svg.querySelector('#' + screen));
		var height = parseFloat(svg.getAttribute('viewBox').split(/\s+/)[3]);
		var getRandomArbitrary = function(min, max) {
		    return Math.random() * (max - min) + min;
		};

		var color = getComputedStyle(svg.querySelector('g:last-of-type rect:last-of-type'));
		color = color.fill;
		function componentToHex(c) {
			var hex = c.toString(16);
			return hex.length == 1 ? "0" + hex : hex;
		}
		function rgbToHex(r, g, b) {
			return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
		}
		var shadeRGBColor = function(color, percent) {
		    var f=color.split(","),
		    	t=percent<0?0:255,
		    	p=percent<0?percent*-1:percent,
		    	R=parseInt(f[0].slice(4)),
		    	G=parseInt(f[1]),
		    	B=parseInt(f[2]);

		    	R = (Math.round((t-R)*p)+R).toString(16);
				G = (Math.round((t-G)*p)+G).toString(16);
				B = (Math.round((t-B)*p)+B).toString(16);
		    return '#' + R + "" + G + "" + B ;
		};
		var hexToRGB = function(hex){
			var r = hex >> 16;
			var g = hex >> 8 & 0xFF;
			var b = hex & 0xFF;
			return "rgb("+r+","+g+","+b+")";
		}
		var cloneRow = function(screen){
			var row = svg.querySelector('#'+screen+' > g:last-of-type').cloneNode(true);
			svg.querySelector('#'+screen).appendChild(row);
			var items = svg.querySelectorAll('#'+screen+'> g:last-of-type rect');
			[].forEach.call(items,function(item, i){
				item.setAttribute('y', parseFloat(item.getAttribute('y')) + parseFloat(item.getAttribute('height') * 2));	
				
				var amount = getRandomArbitrary(0,0.15);
				if (color.charAt(0) != 'r'){
					color = hexToRGB(parseInt(color.slice(1),16));
				}
				items[i].style.fill = shadeRGBColor(color, amount);
			});
			h1 = parseFloat(svg.querySelector('#'+screen).getBoundingClientRect().height);
			vheight = parseFloat(svg.getAttribute('viewBox').split(/\s+/)[3]);
			vw = svg.getAttribute('viewBox').split(/\s+/)[2];
			
			var w = parseFloat(svg.getAttribute('width'));
			var vh = (h1/ w) * vw;
			svg.setAttribute('viewBox', svg.getAttribute('viewBox').replace(vheight, vh));

			svg.setAttribute('height', h1);

			div.style.height = h1 + 'px';
			holder.style.height = h1 + 'px';
		};


		var h1 = svg.querySelector('#'+screen).getBoundingClientRect().height;
		
		while ((h1 < height) || (h1 < mH)){
			cloneRow(screen);
		}
		cloneRow(screen);
		cloneRow(screen);
		mW = (d.documentElement.scrollWidth < w.innerWidth)?d.documentElement.scrollWidth:w.innerWidth;
		svg.setAttribute('width',mW);

	};


	var shapeLocation = function(d, w, svg){
		var screen = 'desktop';
		if (window.innerWidth < 600){
			screen = 'mobile';
		}else if (window.innerWidth < 960){
			screen = 'tablet';
		}
		console.log(screen);
		var rect = svg.querySelector('#'+screen+' > g:last-of-type > rect:last-of-type');
		var x = rect.getBoundingClientRect().width;
		var y = rect.getBoundingClientRect().height;
		var shapes = document.querySelectorAll('.shape');
		[].forEach.call(shapes, function(shape) {
			shape.style.top = y * parseFloat(shape.getAttribute('data-'+screen+'-y')) + 'px';
			shape.style.left = (x/2) * parseFloat(shape.getAttribute('data-'+screen+'-x')) + x/200+ 'px';
			shape.style.width = (x) * parseFloat(shape.getAttribute('data-'+screen+'-width')) + 'px';
		});

	}
	var div = d.querySelector('div.bg');
	var svg = d.querySelector('div.bg svg');
	
	svg.setAttribute('data-viewbox', svg.getAttribute('viewBox'));

	svgFix(div, svg, d, w);
	// setTimeout(function(){
		shapeLocation(d,w,svg);		
	// }, 250);
	


	var resizeTimer;
	w.addEventListener("resize",function(){
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function(){
			maxHeight = (d.documentElement.scrollHeight < w.innerHeight)?w.innerHeight:d.documentElement.scrollHeight;
			svgFix(div, svg,d, w, maxHeight);
			shapeLocation(d,w,svg);
		},250);
	});
}

var Tooltip = function(d,w){
	var links = d.querySelectorAll('a[data-tooltip], a[data-tooltip="true"]');
	var getOffset = function (evt) {
	  var el = evt.target,
	      x = 0,
	      y = 0;

	  while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
	    x += el.offsetLeft - el.scrollLeft;
	    y += el.offsetTop - el.scrollTop;
	    el = el.offsetParent;
	  }

	  x = evt.clientX - x;
	  y = evt.clientY - y;

	  return { x: x, y: y };
	};
	var holder = d.createElement('span');
	holder.classList.add('tooltip__holder');
	d.querySelector('section').appendChild(holder);

	[].forEach.call(links, function(link) {
		
		var moveTimer;
		link.setAttribute('data-tooltip',link.getAttribute('title'));
		link.setAttribute('title','');

		link.addEventListener('mouseenter',function(e){
			console.log(holder);
			// link.classList.add('hover');
			holder.textContent = link.getAttribute('data-tooltip');
			holder.classList.add('active');
			var top = pageYOffset || (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop);
			holder.style.top = e.clientY + top - 20 + 'px';		
			holder.style.left = e.clientX + 18 + 'px';
		});

		link.addEventListener('mousemove',function(e){
			var top = pageYOffset || (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop);
			holder.style.top = e.clientY + top - 20 + 'px';		
			holder.style.left = e.clientX + 18 + 'px';
		});

		link.addEventListener('mouseleave',function(){
			// link.classList.remove('hover');
			holder.classList.remove('active');
		});
	});
};
;(function(w, d, s, t){
	// set use strict to make code that little bit tighter
	'use strict'

	s(d,w);	

	// Touch friendly
	if (('ontouchstart' in window)
      || (navigator.MaxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0)){
		d.documentElement.classList.add('touch');
	}

	var shapeLinks = d.querySelectorAll('.touch .shape:not(.hover) a');
	console.log(shapeLinks);
	[].forEach.call(shapeLinks, function(link){
		link.addEventListener('click',function(e){
			e.preventDefault();
			if (d.querySelectorAll('.touch .shape.hover').length){
				d.querySelectorAll('.touch .shape.hover').classList.remove('hover');	
			}
			link.parentNode.classList.add('hover');
		});
	});
	// Will create external function to handle this but for now will use this

	var addRule;

	if (typeof document.styleSheets != "undefined" && document.styleSheets) {
	    addRule = function(selector, rule) {
	        var styleSheets = document.styleSheets, styleSheet;
	        if (styleSheets && styleSheets.length) {
	            styleSheet = styleSheets[styleSheets.length - 1];
	            if (styleSheet.addRule) {
	                styleSheet.addRule(selector, rule)
	            } else if (typeof styleSheet.cssText == "string") {
	                styleSheet.cssText = selector + " {" + rule + "}";
	            } else if (styleSheet.insertRule && styleSheet.cssRules) {
	                styleSheet.insertRule(selector + " {" + rule + "}", styleSheet.cssRules.length);
	            }
	        }
	    }
	} else {
	    addRule = function(selector, rule, el, doc) {
	        el.appendChild(doc.createTextNode(selector + " {" + rule + "}"));
	    };
	}

	function createCssRule(selector, rule, doc) {
	    doc = doc || document;
	    var head = doc.getElementsByTagName("head")[0];
	    if (head && addRule) {
	        var styleEl = doc.createElement("style");
	        styleEl.type = "text/css";
	        styleEl.media = "screen";
	        head.appendChild(styleEl);
	        addRule(selector, rule, styleEl, doc);
	        styleEl = null;
	    }
	};
	// Internet Exploror
	console.log(navigator.userAgent.toLowerCase());
	if ((navigator.userAgent.toLowerCase().indexOf('msie') > -1) || (navigator.userAgent.toLowerCase().indexOf('trident') > -1)){
		console.log('ie');
		var clips = d.querySelector('.clips defs');
		var shapes = d.querySelectorAll('.shape');
		[].forEach.call(shapes,function(shape, i){
			console.log('shape',i);
			//shape.querySelector('img').addEventListener('load',function(){
				console.log('loaded');
				var h = shape.offsetHeight;
				var w = shape.offsetWidth;
				
				var original = shape.querySelector('a');
				var link = document.createElementNS('http://www.w3.org/2000/svg','a');
				var svg = document.createElementNS('http://www.w3.org/2000/svg','svg');
				var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
				
				// svg.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
				svg.setAttribute('height',h);
				svg.setAttribute('width',w);

				var defs = document.createElementNS('http://www.w3.org/2000/svg','defs');
				var path = document.createElementNS('http://www.w3.org/2000/svg','clippath');

				svgimg.setAttribute('height',h);
				svgimg.setAttribute('width',w);
				svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href',shape.querySelector('img').getAttribute('src'));
				svgimg.setAttribute('x','0');
				svgimg.setAttribute('y','0');


				var clip = getComputedStyle(shape, null).getPropertyValue('clip-path');
				console.log('asdf',clip);
				if (clip.indexOf('url') > -1){
					clip = clip.split("\"");
					clip = clip[1].split("#");
					clip = clip[1];
					// link.setAttributeNS('http://www.w3.org/2000/svg', 'clip-path','url(#'+clip+')');
					var svgclip = clips.querySelector('#'+clip).cloneNode(true);
					svgclip.setAttribute('id',clip+i);
					var polygon = svgclip.querySelector('polygon');
					var points = polygon.getAttribute('points');
					
					points = points.split(" ");
					var newPoints = [];
					points.forEach(function(point){
						point = point.split(",");
						point[0] = (parseFloat(point[0]) / 100) * w;
						point[1] = (parseFloat(point[1]) / 100) * h;
						//console.log(point);
						newPoints.push(point.join(","))
					});
					points = newPoints.join(" ");
					

					polygon.setAttribute('points', points);

					defs.appendChild(svgclip);
					svg.appendChild(defs);
					link.setAttribute('clip-path','url(#'+clip+i+')');
				}

				link.setAttributeNS('http://www.w3.org/1999/xlink','href',shape.querySelector('a').getAttribute('href'));
				// link.setAttributeNS('http://www.w3.org/2000/svg','clip-path','url(#sShape)');
				link.setAttribute('x','0');
				link.setAttribute('y','0');
				link.setAttribute('data-tooltip',true);
				link.setAttribute('title',original.getAttribute('data-tooltip'));

				svg.appendChild(link);
				link.appendChild(svgimg);
				
				original.style.visibility = 'hidden';
				original.style.pointerEvents = 'none';
				shape.classList.add('no__shape');
				shape.appendChild(svg);
			// });
		});
		w.addEventListener('resize',function(){
			// console.log('clip resize');
			setTimeout(function(){
				document.location.reload();
			},100);
		});
	}

	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
		// Make firefox work
		var clips = d.querySelector('.clips defs');
		var shapes = d.querySelectorAll('.shape');
		[].forEach.call(shapes,function(shape, i){
			var clip = getComputedStyle(shape, null).getPropertyValue('clip-path');
			var height = shape.offsetHeight;
			var width = shape.offsetWidth;
			if (clip.indexOf('url') > -1){
				clip = clip.split("\"");
				clip = clip[1].split("#");
				clip = clip[1];
				//console.log(clip);
				var svgClip = d.querySelector('#'+clip).cloneNode(true);
				svgClip.setAttribute('id',clip+i);
				svgClip.classList.add('dynamic');
				// svgClip.setAttribute('clipPathUnits','objectBoundingBox');
				shape.setAttribute('data-clip', clip+i);
				var polygon = svgClip.querySelector('polygon');
				var points = polygon.getAttribute('points');
				
				svgClip.setAttribute('data-points', points);
				
				points = points.split(" ");
				var newPoints = [];
				points.forEach(function(point){
					point = point.split(",");
					point[0] = (parseFloat(point[0]) / 100) * width;
					point[1] = (parseFloat(point[1]) / 100) * height;
					//console.log(point);
					newPoints.push(point.join(","))
				});
				points = newPoints.join(" ");
				
				polygon.setAttribute('points', points);

				clips.appendChild(svgClip);
				createCssRule('.shape[data-clip="'+clip+i+'"], .shape[data-clip="'+clip+i+'"] a', "clip-path:url('#"+ clip+i +"')");

				// createCssRule('.shape[data-clip="'+clip+i+'"] a', "clip-path:none;");
			}
		});
		// Resize events

		w.addEventListener('resize',function(){
			// console.log('clip resize');
			setTimeout(function(){
				document.location.reload();
			},100);
		});
	}


		
	t(d,w);
})(window, document, SVG, Tooltip);