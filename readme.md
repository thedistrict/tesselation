# Simple Gulp scaffold

To get this up and running simply run 2 commands

`sudo npm install` - this will install all the node packages required

`bower install` - this will install all the bower packages, like foundation into your repo


## Gulp commands

Gulp has been set up to handle a few commands

`gulp styles` - this compiles all the SCSS files within the `css/src` directory and those linked within it, combining media queries and minifying the output.

`gulp scripts` - compiles all the JS within `js/src` and specified within `Gulpfile.js`

`gulp watch` - continuous script that watches for changes within the js and scss and recompiles them

`gulp` / `gulp default` - runs both `gulp scripts` and `gulp styles`

#### NB

If running `gulp watch` any changes to Gulpfile.js will only take affect when the process is restarted, i.e. press _ctrl + c_ then run `gulp watch` again.
