var gulp = require('gulp'),
sass = require('gulp-sass')
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
cmq = require('gulp-combine-media-queries'),
cssmin = require('gulp-cssmin'),
rename = require("gulp-rename");

gulp.task('scripts', function () {
    gulp.src(['bower_components/modernizr/modernizr.js','js/src/*.js'])
        .on('error', swallowError) //select all javascript files under js/ and any subdirectory
        .pipe(concat('plugin.min.js')) //the name of the resulting file
        .on('error', swallowError) // Swallow errors so doesn't stop running
        .pipe(uglify()) // Minify
        .on('error', swallowError)
        .pipe(gulp.dest('js')) //Output directory

});

gulp.task('styles', function () {
    gulp.src('css/src/*.scss')
      .pipe(sass())
      .on('error', swallowError)
      //.pipe(gulp.dest('css'));
      .pipe(cmq({
        log: true
      }))
      //.pipe(cssmin())
      //.pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('css'));
});


gulp.task('default',['scripts','styles'], function(){});

gulp.task('watch', function() {
  gulp.watch('css/src/*.scss', ['styles']);
  gulp.watch('css/src/*/*.scss', ['styles']);
  gulp.watch('js/src/*.js', ['scripts']);
});

// Custom error function to stop emit(end);
var swallowError = function(error) {
    console.log(error.toString());
}